import React from "react"
import Blogcard from "./Blogcard"


import image1 from "./images/tiger2.png"
import image2 from "./images/tiger3.jpg"
import image3 from "./images/tiger3.jpg"
import image4 from "./images/tiger5.jpg"
import image5 from "./images/tiger6.jpg"
import image6 from "./images/tiger7.jpg"

function Section3(){


    const blogs =[
        {
            id: 1,
            image: image1,
            title: "Conservation Chronicles: Stories of Triumph and Preservation" ,
            description: "Experience the heartwarming tales within our 'Conservation Chronicles' series, where each story unfolds the triumphs and challenges of safeguarding wildlife. Delve into the inspiring narratives highlighting the dedication and impact of conservation efforts, ensuring a thriving future for endangered species."
        },
        {
            id: 2,
            image: image2,
            title: "Habitat Conservation: Preserving Wildlife Sanctuaries and Tiger Reserves" ,
            description: "Wildlife sanctuaries and tiger reserves are vital for preserving biodiversity. They provide essential habitats for endangered species like tigers. Through conservation efforts, we aim to maintain these areas, promoting coexistence between humans and wildlife."
        },
        
        {
            id: 3,
            image: image3,
            title: "Promoting Eco-Friendly Solutions for a Greener Future" ,
            description: "By advocating for renewable energy and reducing carbon footprints, we pave the way for a sustainable future. Embracing eco-friendly practices ensures resource conservation and minimizes environmental impact, fostering a greener, more resilient planet."
        },

        {
            id: 4,
            image: image4,
            title: "Empowering Local Communities: Fostering Conservation Initiatives for Sustainable Development" ,
            description: "Through community engagement and capacity-building programs, local residents become stewards of conservation. By integrating traditional knowledge with modern practices, we empower communities to lead sustainable development initiatives that preserve natural resources for future generations."
        },

        {
            id: 5,
            image: image5,
            title: "Strategies for Ensuring Coexistence and Habitat Protection" ,
            description: "Implementing effective mitigation measures reduces conflicts between humans and wildlife. By establishing buffer zones and providing alternative livelihoods, we safeguard habitats and promote peaceful coexistence, balancing the needs of communities with the conservation of biodiversity."
        },

        {
            id: 6,
            image: image6,
            title: "Revitalizing Forest Ecosystems: Conservation Efforts to Safeguard Biodiversity Hotspots" ,
            description: "Through reforestation and habitat restoration, we enhance ecosystem resilience. Protecting biodiversity hotspots preserves unique species and ecological functions, ensuring the health and vitality of our forests for generations to come."
        }

         
    ]

    const blogCards = blogs.map(blog=>
    <Blogcard key ={blog.id} currentBlog={blog}/>
)

return(
    <div className="container">
        {blogCards}
    </div>
) 
}


export default Section3