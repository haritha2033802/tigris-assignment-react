import React from "react"

function Header(){
    return(
        <header>
        <div className="container">
    <span id="logo">Tigris</span>
    <button id="menuButton">
      <span className="material-symbols-outlined">pets</span>
    </button>
    <nav>
      <ul id="navList">
        <li>
          <a href="#">Home</a>
        </li>
        <li>
          <a href="#">About</a>
        </li>
        <li>
          <a href="#">Service</a>
        </li>
        <li>
          <a href="#">Contact</a>
        </li>
      </ul>
    </nav>
  </div>
  </header>
    )
}

export default Header