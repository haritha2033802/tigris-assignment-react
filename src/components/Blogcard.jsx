import React from "react"

function Blogcard(props){
  

    return(
      
      <div className="blogsList">  
      <article className="blog">
        
        <img src={props.currentBlog.image} alt=""></img>
        <h3>{props.currentBlog.title}</h3>
        <p>
          {props.currentBlog.description}
        
        </p>
      </article>
      
      </div>
         
    )
}

export default Blogcard