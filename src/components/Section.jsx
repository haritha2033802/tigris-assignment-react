import React from "react"

function Section(){
    return(
        <>
        
  <section>
    <div className="container">
      <h1> Roaring Conservation Efforts in Action</h1>
      <p>
        Explore the majestic wilderness of TIGRIS Tiger Reserve, where
        conservation meets adventure. Nestled in the heart of untamed
        landscapes, our sanctuary stands as a testament to the beauty and
        importance of tiger preservation. Join us in safeguarding these
        magnificent creatures and their habitats for generations to come.
      </p>
      <div className="buttonHolder">
        <a className="button buttonPrimary" href="#">
          Book Safari
        </a>
        <a className="button buttonSecondary" href="#">
          Contact
        </a>
      </div>
    </div>
  </section>
  <section id="sectionAbout">
    <div className="container">
      <h2> Discover the Untamed Beauty of TIGRIS Tiger Reserve</h2>
      <p>
        Embark on an unforgettable journey through the pristine wilderness of
        TIGRIS Tiger Reserve. Immerse yourself in the breathtaking scenery and
        diverse wildlife that call this sanctuary home. With every step,
        experience the raw beauty of nature and join us in our mission to
        protect and preserve this precious ecosystem.
      </p>
    </div>
  </section>
 
</>

    )
}

export default Section